#N11.1
lista = ["Carros" , "Motos" , "Barcos" , "Pedestres" , "Bichos"]
print("Os tres primeiros da lista são: " lista[:3])
print("Os tres ultimos da lista são: " lista[3:5])
print("Os centrais da lista são: " lista[2:4])

#N11.2

pizza_hut = ["Calabrisa" , "Pepperoni" , "4 Queijos"]
pizza_hot_paraguai = pizza_hut[:]

pizza_hot_paraguai.append("Americana")

print("Pizza hut : " , pizza_hut)
print("Pizza hut paraguaia:" , pizza_hot_paraguai)

#N11.3
print("Todas pizzas possuem o mesmo valor RS29,99")
pizza_hut2 = ["Calabresa" , "Pepperoni" , "4 Queijos" , "3 Queijo" , "Abençoada"]
print("Estas são as pizzas que temos")
for i in pizza_hut2:
    print(i)