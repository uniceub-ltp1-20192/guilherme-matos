#include <iostream>
using namespace std; 

int main(){
  int i, n, peso, val_peso;
  float nota, media, mepe, sope;

cout << "Quantas notas deseja aplicar ao aluno? ";
cin >> n;

for (i=0; i<n; i++){
	cout << "Digite a nota de 1 a 10: ";
	cin >> nota;
	while(nota<0 || nota>10){
		cout << "O valor foi inserido incorretamente" << endl << "Digite a nota de 1 a 10: ";
		cin >> nota;
	}
	
	cout << "Digite o peso de 1 a 5: ";
	cin >> peso;
	while(peso<1 || peso>5){
		cout << "Peso inserido incorretamente" << endl << "Digite o peso de 1 a 5: ";
		cin >> peso;
	}
	
	
	media += nota;
	val_peso += peso;
	sope += (nota * peso);
}

media /= n;
cout << "A media e: " << media << endl;
mepe = sope / val_peso;
cout << "A media ponderada e: " << mepe << endl;

system("pause");
return  0;

}
