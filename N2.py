#N2.1 Programa duas variaveis + valor + resultado
qtd_cebolas = int(input("Quantidade de cebolas: "))
val_cebolas = int(input("Valor das cebolas: "))
resultado = qtd_cebolas * val_cebolas

print("Voce ira pagar : " resultado)

#N2.2 Comentarios e correção de erro

cebolas = 300 #Numero total de cebolas
cebolas_na_caixa = 120 #Numero de cebolas na caixa
espaco_caixa = 5 #Espaço da caixa
caixas = 60 #Numero de caixas disponivel
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa #Cebolas total menos as que estão na caixa resultam no numero de cebolas fora da caixa
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa) #cebolas na caixa dividido pelo espaço de cada caixa subtraido do numero de caixas resulta no numero de caixas vazias
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa #cebolas que estao fora da caixa dividido pelo espaço resultando no numero necessario de caixas

#Prints na tela, no programa original não tinha parenteses
print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
print ("Ainda temos,", caixas_vazias, "caixas vazias")
print ("Então, precisamos de", caixas_necessarias, '''caixas para
empacotar todas as cebolas''')
